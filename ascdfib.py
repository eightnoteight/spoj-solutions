fibs = [0, 1]
for x in xrange(1000005):
    fibs.append(fibs[-1] + fibs[-2])
for x in xrange(int(raw_input())):
    a, b = map(int, raw_input().split())
    print 'Case {}:'.format(x + 1),
    y = a
    o = 0
    last = -1
    while o < 100:
        if y > a + b:
            break
        t = fibs[y - 1]
        if last <= t:
            last = t
            print t,
            o += 1
        y += 1
    #for z in sorted([fibs.fib(y - 1) for y in xrange(a, min(a+100, a+b+1))]):
    #    print z,
    print
